---
layout: default
---
<a href="https://steam-boat.gitlab.io/about/"><img src="../assets/logo.png" width="100" ></a> `STEAMboat` is a Popular Science series initiated by [HBCSE, TIFR](https://www.hbcse.tifr.res.in/)

If you are interested in getting updates about our next #STEAMboat session and other activities of HBCSE, do join our mailing list by filling [this form.](https://docs.google.com/forms/d/e/1FAIpQLSe11Gru3VtZLMnutEfX7mwO5bPcyC5xf2x97_WKthUnLojkfg/viewform) or the [Telegram Group](https://t.me/+Im_gitlzPHM3M2E9).


HBCSE Youtube Channel: [Link](https://www.youtube.com/c/HBCSETIFRMumbai)


## Upcoming Session

### S2E3 ट्रैफिक सिग्नल की रंगीन दुनिया: लाल, पीला, हरा!!

![](./assets/S2E3.jpg)

- #### Date: 08 January, 2023 (Sunday)
- #### Time: 11:00 AM IST
- #### Zoom webinar: [Link](https://hbcse-tifr-res-in.zoom.us/j/92397391583?pwd=L0xydWZOYXNXd2Z2WTNJelk0SEZSUT09)
- #### Youtube: [Link](https://youtu.be/X-PRFG8Fy5M)

`About the Talk:`
लाल, पीला, हरा! हर शहर में ट्रैफिक से तो लोग परेशान होते ही हैं, लेकिन, क्या आपने कभी सोचा है, कि ये जाना-पहचाना नजारा - सिग्नल के लाल-पीली-हरी बत्तियां - हमारे जीवन में कैसे आए? चलिये, इस स्टीमबोट सेशन में ट्रैफिक लाइट या ट्रैफिक सिग्नल के इतिहास, डिजाइन और विकास पर एक नजर डालते हैं!

The live session will be in **Hindi**.

**Colourful world of ‘Traffic Signal’: Red, Yellow, Green!**
Red, yellow, green! Traffic and traffic signals are a familiar sight in any big city. We spend perhaps several hours each year waiting at traffic signals. While we hate the traffic, let’s stop and think about how these red-yellow-green lights came about in our lives? In this STEAMboat session, we will have a look at the history, design and evolution of traffic lights in our society and what the future holds for them.

`About the Speaker:`  
**AmisH Parmar**  works with the Vigyan Pratibha team at HBCSE. He has completed his Masters in Physics and has a keen interest in Nuclear physics, Psychology, Philosophy and Library Sciences. He is also an avid podcaster, food reviewer and spends his free time volunteering and board-gaming.


---

## Past Sessions

## S2E2 The Artificial Stone - Concrete

![](./assets/S2E2.png)

- #### Date: 11 December, 2022 (Sunday)
- #### Time: 11:00 AM IST
- #### Zoom webinar: [Link](https://hbcse-tifr-res-in.zoom.us/j/92397391583?pwd=L0xydWZOYXNXd2Z2WTNJelk0SEZSUT09)
- #### Youtube: [Link](https://youtu.be/ULqf9_41lvA)

`About the Talk:`
We live in ‘artificial stone caves’! Amazingly, the stone we use can be molded into any shape we wish, allowing us to build caves (homes) in any size and shape!  Well, the ‘artificial stone’ is ‘concrete’, a fundamental material which is building our civilization. This STEAMboat session will focus on it’s history, chemistry and some interesting anecdotes from the recent research.  Join us to explore the material that creates foundations of the world we live in!

The live session will be in **English**.

`About the Speaker:`  
**Dr. Sathish C. G.**  is a Visiting Fellow at the Homi Bhabha Centre for Science Education (HBCSE). He obtained his PhD in Chemistry from the University of Muenster, Germany, where he studied some light emitting molecules. He is currently involved in chemistry education research at HBCSE. He is interested in understanding the chemistry of materials and the nature around us.


### S2E1 Science of Sleep

![](./assets/S2_E1.png)

- #### Date: 13 November, 2022 (Sunday)
- #### Time: 11:00 AM IST
- #### Zoom webinar: [Link](https://hbcse-tifr-res-in.zoom.us/j/92397391583?pwd=L0xydWZOYXNXd2Z2WTNJelk0SEZSUT09)
- #### Youtube: [Link](https://youtu.be/iMYrDcbCrHo)

`About the Talk:`
One thing that we look forward to, after a long and tiring day, is a good night’s sleep! Sleep relaxes and rejuvenates us like nothing else! So, what is sleep? Why is it so necessary? What exactly happens in our brains and bodies while we sleep? What will happen if we do not get enough sleep? Let's connect in the upcoming STEAMboat session to explore this phenomenon that is so common yet so unknown and mysterious to us!

The live session will be in **English**.

`About the Speaker:`  
**Megha Chougule** is Project Scientific Assistant with D&T group. Megha has completed a 5 year Dual Degree BS-MS Programme from Indian Institute of Science Education and Research (IISER) Pune. Her interests include making physics exciting for school students, exploring connects of nature and science, and promoting scientific literacy.


### E12 खगोलीय घटना व निरीक्षणे 

![](./assets/12.jpg)

- #### Date: 9 October, 2022 (Sunday)
- #### Time: 11:00 AM IST
- #### Zoom webinar: [Link](https://hbcse-tifr-res-in.zoom.us/j/92397391583?pwd=L0xydWZOYXNXd2Z2WTNJelk0SEZSUT09 )
- #### Youtube: [Link](https://youtu.be/11Ztv8BUnIQ)

`About the Talk:`
पावसाळा संपत आलाय (बाहेर बघून तसं वाटत नाहीयं, पण तरी..)! आकाश निरीक्षक आता शरद ऋतुतल्या चंदेरी रात्रींची आतुरतेनं वाट बघतायत. तुम्हालाही आकाशातले ग्रह, तारे आणि त्यांचे विलोभनीय खेळ बघायला आवडतात ना? या वर्षीच्या उरलेल्या तीन महिन्यांत आकाशात घडणाऱ्या खास घटनांची यादी तर लांबलचक आहे. ग्रहणं, गुरूचं खास दर्शन, उल्कावर्षाव, आणि बरंच काही! तर आपल्या येत्या STEAM Boat मध्ये आपण ह्या आगामी घटना आणि त्यांना कसं आणि कुठून बघता येईल याचा आढावा घेऊया.

The end of the monsoon marks the beginning of clear night skies. It is a time when astronomers and sky gazers gear up for the clear star-studded winter nights and all the exciting phenomena that comes with it! And who doesn't love to see the stars, planets and eclipses? Over the next 3 months, we have some spectacular astronomical events like the eclipse of the sun and the moon, meteor shower and there are also planets to look forward to. We will discuss these and also some tips on how to go about our observations and prepare for them. Let’s meet in the upcoming STEAMboat session to talk about all the mesmerising celestial events that are lined up before the new year begins. Happy Sky Watching!

The live session will be in **Marathi**.

`About the Speaker:`  
**Pritesh Ranadive** is an amateur astronomer and an astronomy educator, currently working at the Homi Bhabha Centre for Science Education, TIFR, Mumbai.

### E11 आइए क्षेत्रफल 'गिने'!

![](./assets/11.jpg)

- #### Date: 11 September, 2022 (Sunday)
- #### Time: 11:00 AM IST
- #### Zoom webinar: [Link](https://hbcse-tifr-res-in.zoom.us/j/92397391583?pwd=L0xydWZOYXNXd2Z2WTNJelk0SEZSUT09 )
- #### Youtube: [Link](https://youtu.be/Su6pZ8et8dI)

`About the Talk:` Let’s ‘count’ areas!

Have you seen ‘dot grids’? They are widely used to make rangoli designs. You might have learned to ‘calculate’ or ‘measure’ areas of certain simple geometrical shapes like squares and triangles.  Do you know that we can use these dot grids to find areas of complex and asymmetric shapes? Not just that, we will also explore how can we find the area of any shape by simply counting dots!  So join us for this exciting STEAMboat session!

आइए क्षेत्रफल 'गिने'!
क्या आपने कभी 'डॉट ग्रिड' देखे हैं? ये वही है जिनका उपयोग रंगोली डिजाइन बनाने में किया जाता है। आपने सरल आकारों, जैसे की वर्ग और त्रिकोण, का क्षेत्रफल 'मापन करके' या 'सूत्र के प्रयोग से' निकलना सीखा होगा।क्या आप जानते हैं कि इन डॉट ग्रिड का उपयोग करके हम किसी भी जटिल और असममित आकारों के क्षेत्रफल भी ढूंढ सकते हैं? इतना ही नहीं, हम यह भी पता लगाएंगे कि कैसे हम केवल बिंदुओं की गिनती करके किसी भी आकार का क्षेत्रफल ज्ञात कर सकते हैं! तो इस रोमांचक STEAMboat सत्र के लिए हमसे जुड़ें!

The live session will be in **Hindi**.

`About the Speakers:`  
**Sarika** and **Sreeja** are a part of Vigyan Pratibha project at HBCSE. Both of them are fond of making science and mathematics exciting and accessible for students of all ages.

### E10 चला सूक्ष्म जगाकडे बारकाईने पाहूया

![](./assets/10.png)

- #### Date: 14 August, 2022 (Sunday)
- #### Time: 11:00 AM IST
- #### Zoom webinar: [Link](https://hbcse-tifr-res-in.zoom.us/j/92397391583?pwd=L0xydWZOYXNXd2Z2WTNJelk0SEZSUT09 )
- #### Youtube: [Link](https://www.youtube.com/watch?v=VzkDcWdmU-4)

`About the Talk:` Our next #STEAMboat session will have Rafikh talking about "Solving puzzles in nature using [Foldscope](https://foldscope.com/)".

Who doesn't like puzzles? There are many puzzles hidden in our surroundings. Puzzles like where does dust come from? What does it contain? What tools do we use to explore such questions?  How do we go about solving puzzles like this? And most importantly, what can we learn about science and the natural world through it? Join us on an exploratory journey in the upcoming STEAMBoat session! Join us for the upcoming STEAMboat session on 10th July at 11AM.

कोडी सोडवणे कोणाला आवडत नाहीत? आपल्या सभोवताली, दररोजच्या गोष्टीतही खूप सारी कोडी दडलेली आहेत. जसेकी धूळ कुठून येते? त्यात काय काय असते? अशी कोडी कशी सोडवायची? त्यासाठी कोणती साधने वापरायचे? आणि सर्वात महत्वाचे; यातून आपण विज्ञान आणि निसर्गाबद्दल काय शिकू शकतो? पुढच्या स्टीमबोट सेशन मध्ये आपण या सर्व गोष्टींवर चर्चा करूया.

The live session will be in **Marathi**.

If you are interested in getting updates about our next #STEAMboat session and other activities of HBCSE, do join our mailing list by filling [this form.](https://docs.google.com/forms/d/e/1FAIpQLSe11Gru3VtZLMnutEfX7mwO5bPcyC5xf2x97_WKthUnLojkfg/viewform)

`About the Speaker:`  
**Rafikh Shaikh** is a senior research coordinator at CETE, TISS, Mumbai, and a research scholar at HBCSE, TIFR, Mumbai. He studies how children learn science and how technology facilitates it. He is a recipient of the Foldscope, EarthWatch, and Swachhata Sarathi fellowships.


### E9 बारिश की बूँदें

![](./assets/9.png)

- #### Date: 10 July, 2022 (Sunday)
- #### Time: 11:00 AM IST
- #### Zoom webinar: [Link](https://hbcse-tifr-res-in.zoom.us/j/92397391583?pwd=L0xydWZOYXNXd2Z2WTNJelk0SEZSUT09 )
- #### Youtube: [Link](https://youtu.be/Cd5OYlSLkIo)

`About the Talk:` Our next #STEAMboat session will have Disha share with us the shape of raindrops.

Monsoons bring a smile to all, the young and the old. It is fun to play in the rains and the pitter-patter is a music we all love listening to. Have you ever taken a moment to look closely at the raindrops? What happens to them when they fall from the sky to the ground? Have you ever thought about what is the shape of a raindrop? In this session, we will explore the shapes of raindrops and the reasons for it! Join us for the upcoming STEAMboat session on 10th July at 11AM.

बारिश ...... वो ऋतू जो बच्चो से लेकर बूढ़ों के चेहरे पर मुस्कान लाती है। बारिश के मौसम में पूरी तरह भीग कर उसकी बूंदों के साथ खेलना सबको पसंद है। पर क्या आपने कभी इन्ही बूंदो को गौर से देखा है ? कैसा होता होगा उनका सफर आसमान से ज़मीन तक ? कैसे वो बादलों में से गिरकर छत पे से टपकते हुए, फूलों पत्तों को छूकर, ज़मीन में मिल जाते हैं। तो अगले स्टीमबोट सेशन में हम इन्ही सारी दिलचस्प बातों पर चर्चा करेंगे।

The live session will be in **Hindi** Versions in other languages will also be made available later on HBCSE YouTube channel in the following weeks. The links will be available on this website.

If you are interested in getting updates about our next #STEAMboat session and other activities of HBCSE, do join our mailing list by filling [this form.](https://docs.google.com/forms/d/e/1FAIpQLSe11Gru3VtZLMnutEfX7mwO5bPcyC5xf2x97_WKthUnLojkfg/viewform)

`About the Speaker:`  
**Disha Dbritto** did her post graduation in Physics with specialisation in Solid State Devices. In the past, she worked as a Physics-Maths Educator for a secondary school. Her core interest lies in fostering curiosity and helping students explore the world around them.

---

### E8 Flying buses, superhuman strength, invisible people: Where's the Physics?

![](./assets/8.png)

- #### Date: 12 June, 2022 (Sunday)
- #### Time: 11:00 AM IST
- #### Youtube: [Link](https://www.youtube.com/watch?v=XRPDEV0y368)

`About the Talk:` We all watch and enjoy movies. People achieve far-fetched feats and cars jump from one place to another in an unrealistic fashion; and let us be honest- we all enjoy it and may also believe a part of it to be true! Some of these movie scenes cross the valid boundaries of science and show us many unbelievable events. In this session, we will try to break down some movie scenes, do some quick calculations and check whether they follow the rules of physics. So let’s together explore the world of movies and science, in this #STEAMboat session!

The live session will be in **English and Marathi.** Versions in other languages will also be made available later on HBCSE YouTube channel in the following weeks. The links will be available on this website.

If you are interested in getting updates about our next #STEAMboat session and other activities of HBCSE, do join our mailing list by filling [this form.](https://docs.google.com/forms/d/e/1FAIpQLSe11Gru3VtZLMnutEfX7mwO5bPcyC5xf2x97_WKthUnLojkfg/viewform)


`About the Speaker:`  
**Shirish** works in the physics olympiad cell of HBCSE and enjoys doing his physics education research at the undergraduate level. Developing physics experiments and interacting with students/teachers on experimental physics is something that he is passionate about.

---

### E7 Seeing sounds!

![](./assets/7.png)

- #### Date: 08 May, 2022 (Sunday)
- #### Time: 11:00 AM IST
- #### Youtube: [Hindi](https://www.youtube.com/channel/UCBoz08Kb4GiVIanFv8VUwmg)

`About the Talk:` Throughout our day, we are exposed to a wide variety of sounds. Even when we communicate with others, we majorly rely on sound waves to do so. Usually, we just ‘hear’ these sounds. But is it possible to ‘see’ them as well? Let us find out in the upcoming session of #STEAMboat! We will also try to understand how one can identify different sounds, by using some special properties of sound. And lastly, we will also have a look at some interesting phenomena related to sound! 

The live session will be in **Hindi.** Versions in other languages will also be made available later on HBCSE YouTube channel in the following weeks. The links will be available on this website.

If you are interested in getting updates about our next #STEAMboat session and other activities of HBCSE, do join our mailing list by filling [this form.](https://docs.google.com/forms/d/e/1FAIpQLSe11Gru3VtZLMnutEfX7mwO5bPcyC5xf2x97_WKthUnLojkfg/viewform)

`About the Speaker:`  
**Pankaj Tadakhe** is a part of the Vigyan Pratibha Project at HBCSE and is working in the area of developing educational resources for higher secondary classes. He likes to look out for science and mathematics in our surroundings and day to day life activities. 

---

### E6 Games and Magic Square!

![](./assets/6.png)

- #### Date: 10 Apr, 2022 (Sunday)
- #### Time: 11:00 AM IST
- #### Youtube: [Hindi](https://www.youtube.com/watch?v=1d_3_Gk8GE0)

`About the Talk:` You may have played games such as tic-tac-toe that are simple yet exciting! Many of your notebooks might be filled with games of X and O. How about designing and exploring some more games but with Magic Squares? This will be an introductory episode, where we will together explore an intuitive approach to solving a 3*3 magic squares. We will then look at a few interesting games based on magic square and discuss ways in which we can make our own games, using the same logic! This episode will be most suited for middle school students and educators teaching in grades 7-9, though everyone who loves solving puzzles and games are welcome to join the voyage and contribute actively to the discussion! 

The live session will be in **Hindi.** Versions in other languages will also be made available later on HBCSE YouTube channel in the following weeks. The links will be available on this website.

If you are interested in getting updates about our next #STEAMboat session and other activities of HBCSE, do join our mailing list by filling [this form.](https://docs.google.com/forms/d/e/1FAIpQLSe11Gru3VtZLMnutEfX7mwO5bPcyC5xf2x97_WKthUnLojkfg/viewform)

`About the Speaker:`  
**Ravi** works in the area of STEAM education research at HBCSE. He enjoys exploring toys, fun activities, and games and thinking of ways to integrate these in classrooms to make learning more engaging.

---

### E5 Rango ka khel!

![](./assets/5.png)

- #### Date: 13 Mar, 2022 (Sunday)
- #### Time: 11:00 AM IST
- #### Youtube: [Hindi](https://www.youtube.com/watch?v=5j-aRR90cLE)

`About the Talk:` As we prepare for Holi, the festival of colour, let's ask ourselves what is colour after all?
We live in a world bathed in light, and at least most of us can see the various colours of objects that make our world so beautiful. But what is colour anyway? Is there pink in a rainbow? Why do two people see the same colour differently? Well, colour is all in our head!
While light has intrinsic physical properties such as wavelength and intensity, colour is not actually a property of light or of objects that reflect light but entirely a sensation that arises in our brain, due to a complex chain of events starting from when light enters the eye. And the brain can even be fooled to see colours that don’t exist!
Join us in our Holi special as we dive into the world of colour! 

The live session will be in **Hindi.** Versions in other languages will also be made available later on HBCSE YouTube channel in the following weeks. The links will be available on this website.

If you are interested in getting updates about our next #STEAMboat session and other activities of HBCSE, do join our mailing list by filling [this form.](https://docs.google.com/forms/d/e/1FAIpQLSe11Gru3VtZLMnutEfX7mwO5bPcyC5xf2x97_WKthUnLojkfg/viewform)

`About the Speaker:`  
**Arnab Bhattacharya** is a scientist, science communicator and tinkerer at TIFR’s Homi Bhabha Centre for Science Education.

---

### E4 Let’s Measure! Oh, is that so Simple?

![](./assets/4.png)

- #### Date: 13 Feb, 2022 (Sunday)
- #### Time: 11:00 AM IST
- #### Recording(on youtube): [Marathi](https://www.youtube.com/watch?v=vojkV10P6PM); [Hindi](https://www.youtube.com/watch?v=uofJO1gkd_8); [English](https://www.youtube.com/watch?v=3xgwieZ_1dQ)

`About the Talk:` Measurements are an integral part of everyone’s life. We have become so accustomed to the idea of measuring and quantification that nowadays, our happiness and anxiety also get a number! Practices of measurement come from historical agreements, where we compare an unknown quantity with a known or standard one when we measure. This talk will illustrate the historical development of basic measurements such as length and area in the world and Indian context. Then we will unpack a chapter of history on measuring the volume of a sphere to understand the role of accuracy in measurement. Lastly, we will see how the development of standardised units is represented in school mathematics and examine some students’ practices wherein their after-school jobs required them to constantly make negotiations to achieve accuracy.

The live session will be in **Marathi.** Versions in other languages will also be made available later on HBCSE YouTube channel in the following weeks. The links will be available on this website.

If you are interested in getting updates about our next #STEAMboat session and other activities of HBCSE, do join our mailing list by filling [this form.](https://docs.google.com/forms/d/e/1FAIpQLSe11Gru3VtZLMnutEfX7mwO5bPcyC5xf2x97_WKthUnLojkfg/viewform)


`About the Speaker:`  
**Dr. Shweta Naik** is a researcher in mathematics education and believes in teaching mathematics with justice. She is a Scientific Officer at the Homi Bhabha Centre for Science Education. Her research intends to develop materials for teaching mathematics that take into account the social contexts, history and interdisciplinary connections of math ideas.

---

### E3 Patterns in a Calendar!

![](./assets/3.png)

- #### Date: 09 Jan, 2022 (Sunday)
- #### Time: 11:00 AM IST
- #### Recording(on youtube): [Hindi](https://youtu.be/68xK1suV-2Q); [Gujarati](https://youtu.be/JSJ9S32GaNU)

`About the Talk:` We all love uncovering patterns, whether it's deciphering a movie plot or figuring out puzzles, playing chess, or solving crosswords. Have you ever wondered what kind of patterns you might find in the calendar? We invite you to join us on the next STEAMboat voyage, to experience the joy of unfolding  the hidden mysteries embedded within this seemingly simple structure of numbers. 

The session is open to all those who are interested in connecting mathematics with their surroundings. The discussions will resonate with students and educators from the  middle and high school (Grade 6-10) space. We look forward to your participation.

The live session will be in **Hindi.** Versions in other languages will also be made available later on HBCSE YouTube channel in the following weeks. The links will be available on this website.

If you are interested in getting updates about our next #STEAMboat session and other activities of HBCSE, do join our mailing list by filling [this form.](https://docs.google.com/forms/d/e/1FAIpQLSe11Gru3VtZLMnutEfX7mwO5bPcyC5xf2x97_WKthUnLojkfg/viewform)


`About the Speaker:`  
**Harita Raval** is an education researcher and communicator working in the area of school mathematics education at HBCSE. She is passionate about developing and understanding hands-on mathematics activities for teachers and students. 

---

### E2 Here comes the Sun!

![](./assets/2.png)

- #### Date: 12 Dec, 2021 (Sunday)
- #### Time: 11:00 AM IST
- #### Recording(on youtube): [English](https://www.youtube.com/watch?v=FO2ecJIVOoY); [Marathi](https://www.youtube.com/watch?v=9dqUp6nPGtY)

`About the Talk:` We all know that the sun rises in the east and sets in the west … but when was the last time you actually watched sunrise or sunset? What if we told you that it didn’t rise at the east or set at the west today?! This sounds a bit unbelievable, so we encourage you to go out and check for yourself! Then, join us for the next session of STEAMboat, where we look at where the sun actually rises and sets. And we try to understand why this happens by imagining how the sun moves through the sky. Along the way, we see that how the sun moves in the sky depends on where you are on the earth, and on what time of the year it is. 

`About the Speakers:`  
**Varuni P.** is an Outreach Associate at The Institute of Mathematical Sciences (IMSc), Chennai. She works on designing and conducting activities that make science communication more interactive. She also works on modeling how cyanobacteria move and on many (unfinished) fiber craft projects.    
**Chaitanya Ursekar** is a PhD student at HBCSE, where he designs science and maths activities for students and works on understanding how teachers use those activities in their classrooms. 

---

### E1 The Superb Spider’s Super Silk

![](./assets/1.png)

- #### Date: 14 Nov, 2021 (Sunday)
- #### Time: 11:00 AM IST
- #### Recording(on youtube): [English](https://youtu.be/H_q0z8qifSg); [Hindi](https://www.youtube.com/watch?v=WrVXEvcCgxk)

`About the Talk:` Encountering a cobweb on one’s face or hands during a stroll can be quite unpleasant. Still, who isn’t impressed by the agility and athletic skills of the superhero ‘Spiderman’. Who says entertainment and science are an unlikely couple? Let’s explore the story of the sci-fiction movie ‘The Amazing Spiderman’ and try to connect it with the research on understanding spider silk - an amazing fibre produced by an equally amazing creature. We’ll look at how spiders, who’ve been around for almost 380 million years, spin this wonder material, and look at some of its unique properties. And we’ll check if Spiderman's artificial silk-producing gadget is a possible reality of science or mere fiction! 

`About the Speaker:` **Dr. Suravi Kalita** is a science enthusiast, cinephile, and a wannabe fiction writer. She is currently a Visiting Fellow at HBCSE, TIFR. Her research interests include wetland ecology and environmental education among others.


