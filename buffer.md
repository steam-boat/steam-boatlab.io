## 2 The Thirsty crow problem

![](./assets/steam-t1.png)

**About the Talk:** In this episode we will explore one of the most famous childhood stories, that of ‘The Thirsty Crow’. Firstly we will try to model the story by making our own volume measuring device using some easily available items. Then, making use of it we will begin our quest of answering the question: ‘Will the thirsty Crow always be able to quench its thirst?’ During this adventure, we will see how to find the volume of small objects by different methods, and how this can be used to understand the scientific concept of ‘packing of solids’. 
To know more, check out the Learning Unit titled ‘An experiment on measuring volume’ on the Vigyan Pratibha website: [Here](https://vigyanpratibha.in/index.php/an-experiment-on-measuring-volume/)

`About the Speaker:` Amish Parmar is part of the Vigyan Pratibha project, at the Homi Bhabha Centre for Science Education, TIFR. He did his post graduation in Physics from the Department of Physics, Savitribai Phule Pune University. Apart from Nuclear Physics, Amish also has interests in Library Sciences, Psychology, and Philosophy. He is an avid podcaster, vlogger, and food reviewer.

- ### Date: 28 Nov, 2021 (Sunday)
- ### Time: 11:00 AM to 11:30 AM
- ### Link to join (Zoom Meet): [Here]()
- ### Youtube Link: [Here](https://www.youtube.com/c/HBCSETIFRMumbai)

---



