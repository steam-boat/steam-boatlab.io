---
layout: page
title: About
permalink: /about/
---

<img src="../assets/logo.png" width="100"> `STEAMboat` is a Popular Science series initiated by [HBCSE, TIFR](https://www.hbcse.tifr.res.in/)

STEAMboat initiative aims to use new media technologies and the internet to widen the outreach endeavour in the digital space. The idea is to reach out to the general audience by having engaging sessions on STEAM and related topics in Indian regional languages as well as English. For starters, a STEAMboat session will be released every second Sunday of the month. These sessions may be live or pre-recorded, and will cover a range of topics from all disciplines which are of general interest to people. The sessions will be available on [HBCSE’s YouTube channel](https://www.youtube.com/c/HBCSETIFRMumbai). For updates do also check out the [twitter handle](https://twitter.com/HBCSE_TIFR)

[Homi Bhabha Centre for Science Education (HBCSE)](https://www.hbcse.tifr.res.in/) is a National Centre of the Tata Institute of Fundamental Research (TIFR), Mumbai. The broad goals of the Centre are to promote equity and excellence in science and mathematics education from primary school to undergraduate college level, and encourage the growth of scientific literacy in the country.


### Press Release

22 Nov, 2021. TOI (English). [Homi Bhabha centre to blend arts with science](https://timesofindia.indiatimes.com/city/mumbai/mumbai-homi-bhabha-centre-to-blend-arts-with-science/articleshow/87837788.cms). \| [TIFR's Homi Bhabha Centre launches online series integrating arts with STEM](https://timesofindia.indiatimes.com/home/education/news/tifrs-homi-bhabha-centre-launches-online-series-integrating-arts-with-stem/articleshow/87840890.cms)

16 Nov, 2021. Loksattha, (Marathi)<img src="../assets/press-16Nov21.jpeg" width="200">

